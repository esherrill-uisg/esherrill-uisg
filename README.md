### Hi there 👋 I'm Eric R. Sherrill in Chicago, IL

- 🔭 I’m currently working on Terraform, Ansible, AWS and Azure
- 🌱 I’m currently learning Ansible
- 👯 I’m looking to collaborate on not much at the moment
- 🤔 I’m looking for help with not much at the moment
- 💬 Ask me about anything
- 📫 How to reach me: esherrill{at}uisg.com
- 😄 Pronouns: he/him/his
- ⚡ Fun fact: I used to be a lawyer back in the 90's

